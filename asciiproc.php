<?php

require 'APDocument.php';
require 'APTransformer.php';

define('BASEDIR', __DIR__);
define('ASCIIPROC_CONFIG_FILE', __DIR__ . '/asciiproc.ini');
define('ASCIIPROC_HTML5_ARTICLE_XSL', __DIR__ . '/html5-article.xsl');
define('ASCIIPROC_DOCBOOK5_ARTICLE_XSL', __DIR__ . '/docbook5-article.xsl');
define('ASCIIPROC_DOCBOOK5_BOOK_XSL', __DIR__ . '/docbook5-book.xsl');

function asciiproc_highlight($source, $language = null, $linenos = null)
{
    $language = $language ? $language[0]->value : null;
    $highlighter = new Highlighter();
    $highlighter->setLanguage($language);
    /*if ($linenos and $linenos[0]->value == 'linenos') {
    }*/
    $output = $highlighter->highlight(trim($source[0]->data, PHP_EOL));
    return $output;
}

function asciiproc_string_transform(string $source, string $stylesheet_file)
{
    $doc = new APDocument();
    $doc->loadAsciidoc($source);
    $xform = new APTransformer();
    $xform->importStyleSheetFromFile($stylesheet_file);
    return $xform->transformToXml($doc);
}

function asciiproc_file_transform(string $source_file, string $stylesheet_file)
{
    $doc = new APDocument();
    $doc->load($source_file);
    $xform = new APTransformer();
    $xform->importStyleSheetFromFile($stylesheet_file);
    return $xform->transformToXml($doc);
}

function asciiproc_string_to_html5_article(string $source = null)
{
    if (is_string($source) and !empty($source)) {
        return asciiproc_string_transform($source, ASCIIPROC_HTML5_ARTICLE_XSL);
    }
}

function asciiproc_file_to_html5_article(string $source_file)
{
    return asciiproc_file_transform($source_file, ASCIIPROC_HTML5_ARTICLE_XSL);
}

function asciiproc_string_to_docbook5_article(string $source = null)
{
    if (is_string($source) and !empty($source)) {
        return asciiproc_string_transform($source, ASCIIPROC_DOCBOOK5_ARTICLE_XSL);
    }
}

function asciiproc_file_to_docbook5_article(string $source_file)
{
    return asciiproc_file_transform($source_file, ASCIIPROC_DOCBOOK5_ARTICLE_XSL);
}

function asciiproc_string_to_docbook5_book(string $source = null)
{
    if (is_string($source) and !empty($source)) {
        return asciiproc_string_transform($source, ASCIIPROC_DOCBOOK5_BOOK_XSL);
    }
}

function asciiproc_file_to_docbook5_book(string $source_file)
{
    return asciiproc_file_transform($source_file, ASCIIPROC_DOCBOOK5_BOOK_XSL);
}
