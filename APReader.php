<?php

class APReader
{
    private $textArray;

    private $textArrayLength;

    private $baseLineNum;

    private $currentLineNum;

    public $lastMatch;

    public function __get($name)
    {
        $line_num = $this->currentLineNum;
        $value = null;
        if ($name == 'currentLineNum')
            return $line_num + 1;
        $exp = "/^(previousLine|currentLine|nextLine)(Text|IsBlank|IsIndented)$/";
        if (preg_match($exp, $name, $match)) {
            switch ($match[1]) {
                case 'previousLine':
                $value = ($line_num > 0) ? $this->textArray[$line_num - 1] : false;
                break;
                case 'currentLine':
                $value = $this->textArray[$line_num];
                break;
                case 'nextLine':
                $value = ($line_num < $this->textArrayLength - 1) ? $this->textArray[$line_num + 1] : false;
                break;
                default:
                break;
            }
            if ($match[2] == 'Text') {
                return $value;
            } elseif ($match[2] == 'IsBlank') {
                return !is_string($value) || empty(trim($value));
            } elseif ($match[2] == 'IsIndented') {
                return ltrim($value) !== $value;
            }
        }
        return $value;
    }

    public function __call(string $name, array $args)
    {
        $this->lastMatch = false;
        $line_num = $this->currentLineNum;
        $value = null;
        $return = false;
        $exp = "/^(previousLine|currentLine|nextLine)(Match|MatchAll)$/";
        if (preg_match($exp, $name, $function_parts)) {
            switch ($function_parts[1]) {
                case 'previousLine':
                //$text = ($line_num > 0) ? $this->textArray[$line_num - 1] : null;
                $text = $this->getTextLine($line_num - 1);
                break;
                case 'currentLine':
                $text = $this->textArray[$line_num];
                break;
                case 'nextLine':
                $text = ($line_num < $this->textArrayLength - 1) ? $this->textArray[$line_num + 1] : null;
                break;
                default:
                $text = null;
                break;
            }
            if ($text) {
                if ($function_parts[2] == 'Match') {
                    if (preg_match('/' . $args[0] . '/' , $text, $match)) {
                        $this->lastMatch = ['full_match' => $match[0]];
                        foreach ($match as $name => $value) {
                            if (is_string($name) && !empty($value)) {
                                $this->lastMatch[$name] = $value;
                            }
                        }
                        //echo "Last match"; printout($this->lastMatch);
                        return true;
                    }
                } elseif ($function_parts[2] == 'MatchAll') {
                    if (preg_match_all('/' . $args[0] . '/' , $text, $match, PREG_OFFSET_CAPTURE | PREG_PATTERN_ORDER)) {
                        $this->lastMatch = ['full_match' => $match[0]];
                        foreach ($match as $name=> $value) {
                            if (is_string($name) && !empty($value)) {
                                $this->lastMatch[$name] = $value;
                            }
                        }
                        return true;
                    }
                }
            }
        }
        $this->lastMatch = null;
        return false;
    }

    public function loadDoc(string $text, int $base_line_num = 0)
    {
        $this->textArray = explode("\n", $text);
        $this->textArrayLength = count($this->textArray);
        $this->baseLineNum = $base_line_num;
        $this->currentLineNum = -1;
    }

    public function refNextLine()
    {
        $num_lines = $this->textArrayLength;
        if ($this->currentLineNum < $num_lines) {
            $this->currentLineNum++;
        }
        return $this->currentLineNum < $num_lines;
    }

    public function refNextNonBlankLine()
    {
        do {
            if (!$this->refNextLine()) {
                return false;
            }
        } while ($this->currentLineIsBlank);
        return true;
    }

    public function refPreviousLine()
    {
        if ($this->currentLineNum == 0) {
            return false;
        }
        $this->currentLineNum--;
        return true;
    }

    public function getCurrentLine()
    {
        if ($this->currentLineNum == -1) {
            $this->currentLineNum = 0;
        }
        #return $this->textArray[$this->currentLineNum];
        return $this->getTextLine($this->currentLineNum);
    }

    function getTextLine($line_num)
    {
        return isset($this->textArray[$line_num]) ? $this->textArray[$line_num] : null;
    }

    function getCurrentLineNumAbs()
    {
        return $this->baseLineNum + $this->currentLineNum + 1;
    }
}
