<?php

class APListElement extends APElement
{
    function __construct(string $name, array $values = null)
    {
        DOMElement::__construct($name);
        $this->values = $values;
    }

    function addLabel(string $text)
    {
        if ($this->body->hasChildNodes() and
            $this->body->lastChild->nodeName == 'listitem') {
            $this->appendElement('body');
        }
        $this->body->appendElement('label', $text);
    }

    function addListItem(string $text = null, int $index = null)
    {

        if ($this->nodeName == 'numberedlist' and $index) {
            $expected_index = $this->body->childNodes->length + 1;
            if ($index !== $expected_index) {
                $this->ownerDocument->writeToLog('W', "expected $expected_index, got $index");
            }
        }
        $listitem = $this->body->appendElement('listitem');
        if ($text) {
            $listitem->appendElement('simpara', $text);
        }
        return $listitem;
    }

    function bodyAppend(DOMNode $element)
    {
        $listitem = $this->body->lastChild;
        if (!$listitem) {
            $listitem = $this->addListItem();
        }
        $listitem->appendChild($element);
        return $element;
    }

    function bodyAppendElement(string $element_name, $content = null, array $values = [])
    {
        $element = $this->ownerDocument->createElement($element_name, $content, $values);
        $this->bodyAppend($element);
        return $element;
    }
}
