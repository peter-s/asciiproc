<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:asciiproc="http://gitlab.com/peter-s/asciiproc"
    exclude-result-prefixes="asciiproc">

<xsl:import href='asciiproc://functions'/>

<xsl:output method="html"
    doctype-system="about:legacy-compat"
    omit-xml-declaration="yes"
    encoding="UTF-8"/>

<!-- Document template -->
<xsl:template match="/*">
    <html>
        <head>
            <title><xsl:value-of select="title"/></title>
            <style type="text/css">
                <xsl:value-of select="asciiproc:include('styles.css')"/>
            </style>
        </head>
        <body>
            <xsl:apply-templates select="section[1]"/>
        </body>
    </html>
</xsl:template>


<!-- Article -->

<xsl:template match="/*/section">
    <article>
        <xsl:apply-templates select="head"/>
        <xsl:apply-templates select="body"/>
        <xsl:if test="//footnote">
            <div class="footnotes">
                <xsl:apply-templates select="//footnote|//footnoteref" mode="footnotes"/>
            </div>
        </xsl:if>
    </article>
</xsl:template>


<!-- Table of contents -->

<xsl:template match="render-toc">
    <div id="toc">
        <nav>
            <h2><xsl:value-of select="/*/toc-title"/></h2>
            <!--<xsl:call-template name="toc"/>-->
            <xsl:apply-templates select="/*/section/body" mode="toc"/>
        </nav>
    </div>
</xsl:template>

<xsl:template match="body" mode="toc">
    <xsl:if test="section">
        <ul>
            <xsl:for-each select="section">
                <li><a href="#{@id}"><xsl:value-of select="title"/></a>
                <xsl:if test="@level &lt; /*/toclevels"><xsl:apply-templates select="body" mode="toc"/>
                </xsl:if>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:if>
</xsl:template>


<!-- Sections -->

<xsl:template match="section">
    <section>
        <xsl:call-template name="block-attributes">
            <xsl:with-param name="class-list">
                <xsl:choose>
                    <xsl:when test="not(title) and @style = 'preface'">preamble</xsl:when>
                    <xsl:otherwise><xsl:value-of select="concat('sect', @level)"/></xsl:otherwise>
                </xsl:choose>
            </xsl:with-param>
        </xsl:call-template>
        <xsl:if test="title">
            <h1 id="{@id}"><xsl:apply-templates select="title/node()"/></h1>
        </xsl:if>
        <div class="sectionbody"><xsl:apply-templates select="body/*"/></div>
    </section>
</xsl:template>


<xsl:template match="section/head">
    <header>
        <xsl:apply-templates select="title"/>
        <xsl:apply-templates select="@id"/>
        <xsl:if test="author">
            <div id="author"><xsl:value-of select="author/text()"/></div>
        </xsl:if>
        <xsl:if test="email">
            <a id="email" href="mailto:{email/text()}"><xsl:value-of select="email/text()"/></a>
        </xsl:if>
        <xsl:if test="revnumber">
            <div id="revision">
                <xsl:value-of select="concat('Version ', revnumber)"/>
                <xsl:if test="revdate"><xsl:text>, </xsl:text><xsl:value-of select="revdate"/></xsl:if>
            </div>
        </xsl:if>
        <xsl:apply-templates select="revremark"/>
    </header>
</xsl:template>

<xsl:template match="revremark">
    <div id="revremark"><xsl:apply-templates /></div>
</xsl:template>

<xsl:template match="/*/section[0]/body">
    <div class="articlebody">
        <xsl:apply-templates />
    </div>
</xsl:template>


<!-- Section title -->
<xsl:template match="head/title|section/title">
    <h1 id="{../../@id}"><xsl:apply-templates /></h1>
</xsl:template>

<xsl:template match="body" mode="section-body">
    <div class="sectionbody"><xsl:apply-templates /></div>
</xsl:template>

<!-- Stop titles appearing in the wrong places -->
<xsl:template match="title"></xsl:template>


<!-- Block ID attribute -->
<xsl:template match="@id">
    <xsl:attribute name="id"><xsl:value-of select="."/></xsl:attribute>
</xsl:template>


<!-- Blocks -->

<xsl:template match="listingblock[@language]">
    <div>
        <xsl:call-template name="block-attributes"/>
        <xsl:apply-templates select="title" mode="blocktitle"/>
        <pre class="{@language}"><code>
            <xsl:value-of select="asciiproc:highlight(body/text(), @language, @linenos)" disable-output-escaping="yes"/>
        </code></pre>
    </div>
</xsl:template>

<xsl:template match="listingblock">
    <div>
        <xsl:call-template name="block-attributes"/>
        <xsl:apply-templates select="title" mode="blocktitle"/>
        <pre><code>
            <xsl:copy-of select="body/text()"/>
        </code></pre>
    </div>
</xsl:template>

<xsl:template match="literalblock">
    <div>
        <xsl:call-template name="block-attributes"/>
            <!--<xsl:with-param name="class-list">literalblock</xsl:with-param>
        </xsl:call-template>-->
        <xsl:apply-templates select="title" mode="blocktitle"/>
        <pre><xsl:value-of select="body/text()"/></pre>
    </div>
</xsl:template>

<xsl:template match="admonitionblock">
    <div>
        <xsl:call-template name="block-attributes">
            <xsl:with-param name="class-list">
                <xsl:value-of select="@name"/>
            </xsl:with-param>
        </xsl:call-template>
        <div class="caption"><xsl:value-of select="@name"/></div>
        <xsl:apply-templates select="title" mode="blocktitle"/>
        <xsl:apply-templates select="body"/>
    </div>
</xsl:template>

<xsl:template match="quoteblock|verseblock">
    <div>
        <xsl:call-template name="block-attributes"/>
        <xsl:apply-templates select="title" mode="blocktitle"/>
        <blockquote>
            <xsl:apply-templates select="body"/>
            <xsl:apply-templates select="attribution"/>
        </blockquote>
    </div>
</xsl:template>

<xsl:template match="sidebarblock">
    <div>
        <xsl:call-template name="block-attributes"/>
        <xsl:apply-templates />
    </div>
</xsl:template>

<xsl:template match="attribution">
    <footer><xsl:apply-templates /></footer>
</xsl:template>

<xsl:template match="citetitle">
    <cite><xsl:copy-of select="text()"/></cite>
</xsl:template>

<xsl:template match="body" mode="unwrapped">
    <xsl:apply-templates />
</xsl:template>

<xsl:template match="body">
    <div class="content"><xsl:apply-templates /></div>
</xsl:template>


<!-- Paragraphs -->

<xsl:template match="simpara">
    <p><xsl:apply-templates /></p>
</xsl:template>

<xsl:template match="para">
    <div>
        <xsl:call-template name="block-attributes"/>
        <xsl:apply-templates select="title" mode="blocktitle"/>
        <p><xsl:apply-templates select="body" mode="unwrapped"/></p>
    </div>
</xsl:template>

<xsl:template match="para[style='literal']">
    <div>
        <xsl:call-template name="block-attributes"/>
        <xsl:apply-templates select="title" mode="blocktitle"/>
        <pre><xsl:apply-templates select="body" mode="unwrapped"/></pre>
    </div>
</xsl:template>


<!-- Block attributes (ID, classes) -->

<xsl:template name="block-attributes">
    <xsl:param name="class-list"/>
    <xsl:if test="name() != 'section'"><xsl:apply-templates select="@id"/></xsl:if>
    <xsl:attribute name="class"><xsl:value-of select="normalize-space(concat(name(), ' ', @style, ' ', $class-list, ' ', @role))"/></xsl:attribute>
</xsl:template>

<!-- Block title -->

<xsl:template match="title" mode="blocktitle">
    <div class="blocktitle"><xsl:apply-templates /></div>
</xsl:template>


<!-- Lists -->

<!-- Bulleted list -->
<xsl:template match="bulletedlist">
    <div>
        <xsl:call-template name="block-attributes"/>
        <xsl:apply-templates select="title" mode="blocktitle"/>
        <ul>
            <xsl:apply-templates select="body"/>
        </ul>
    </div>
</xsl:template>

<!-- Numbered list -->
<xsl:template match="numberedlist|calloutlist">
    <div>
        <xsl:call-template name="block-attributes"/>
        <xsl:apply-templates select="title" mode="blocktitle"/>
        <ol>
            <xsl:apply-templates select="@id|@start"/>
            <xsl:attribute name="type">
                <xsl:choose>
                    <xsl:when test="@style='arabic'">1</xsl:when>
                    <xsl:when test="@style='loweralpha'">a</xsl:when>
                    <xsl:when test="@style='upperalpha'">A</xsl:when>
                    <xsl:when test="@style='lowerroman'">i</xsl:when>
                    <xsl:when test="@style='upperroman'">I</xsl:when>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates/>
        </ol>
    </div>
</xsl:template>

<xsl:template match="listitem">
    <li><xsl:apply-templates/></li>
</xsl:template>

<!-- Labeled list -->
<xsl:template match="labeledlist">
    <div>
        <xsl:call-template name="block-attributes"/>
        <xsl:apply-templates select="title" mode="blocktitle"/>
        <dl><xsl:apply-templates mode="labeledlist"/></dl>
    </div>
</xsl:template>

<xsl:template match="body" mode="labeledlist">
    <xsl:apply-templates mode="labeledlist"/>
</xsl:template>

<xsl:template match="label" mode="labeledlist">
    <dt><xsl:apply-templates /></dt>
</xsl:template>

<xsl:template match="listitem" mode="labeledlist">
    <dd><xsl:apply-templates /></dd>
</xsl:template>

<xsl:template match="@style">
    <xsl:attribute name="class"><xsl:value-of select="."/></xsl:attribute>
</xsl:template>

<!-- Qanda list -->
<xsl:template match="labeledlist[@style='quanda']">
    <div>
        <xsl:call-template name="block-attributes"/>
        <xsl:apply-templates select="title" mode="blocktitle"/>
        <ol>
            <xsl:apply-templates select="body" mode="quandalist"/>
        </ol>
    </div>
</xsl:template>

<xsl:template match="body" mode="quandalist">
    <li><xsl:apply-templates select="label" mode="quandalist"/>
    <xsl:apply-templates select="listitem" mode="quandalist"/></li>
</xsl:template>

<xsl:template match="label" mode="quandalist">
    <p class="question"><xsl:apply-templates /></p>
</xsl:template>

<xsl:template match="listitem" mode="quandalist">
    <xsl:apply-templates />
</xsl:template>


<!-- Link -->

<xsl:template match="link">
    <a href="{@name}:{@target}"><xsl:apply-templates select="@id|@title"/><xsl:value-of select="text()"/></a>
</xsl:template>

<xsl:template match="image[@link]">
    <a href="{@link}">
        <img src="{@target}">
            <xsl:apply-templates select="@alt|@float|@height|@id|@title|@width"/>
        </img>
    </a>
</xsl:template>

<xsl:template match="image">
    <img src="{@target}">
        <xsl:apply-templates select="@alt|@float|@height|@id|@title|@width"/>
    </img>
</xsl:template>

<xsl:template match="@title|@alt|@width|@height|@start">
    <xsl:attribute name="{name()}"><xsl:value-of select="."/></xsl:attribute>
</xsl:template>

<xsl:template match="@float">
    <xsl:attribute name="style">float: <xsl:value-of select="."/></xsl:attribute>
</xsl:template>

<!-- Cross reference -->
<xsl:template match="xref">
    <a href="#{@target}"><xsl:value-of select="text()"/></a>
</xsl:template>

<xsl:template match="indexterm2">
    <xsl:apply-templates />
</xsl:template>


<!-- Footnotes -->

<xsl:template match="footnote">
    <xsl:variable name="footnoteCount">
        <xsl:number level="any" count="footnote"/>
    </xsl:variable>
    <sup><a id="_footnoteref_{$footnoteCount}" href="#_footnote_{$footnoteCount}"><xsl:value-of select="$footnoteCount"/></a></sup>
</xsl:template>

<xsl:template match="footnote2">
    <xsl:apply-templates select="//footnote" mode="footnote2">
        <xsl:with-param name="target"><xsl:value-of select="@target"/></xsl:with-param>
    </xsl:apply-templates>
</xsl:template>

<xsl:template match="footnote" mode="footnote2">
    <xsl:param name="target"/>
    <xsl:if test="@target = $target">
        <sup><a id="_footnoteref_{position()}" href="#_footnote_{position()}"><xsl:value-of select="position()"/></a></sup>
    </xsl:if>
</xsl:template>

<xsl:template match="footnote" mode="footnotes">
    <p id="_footnote_{position()}"><a class="footnote" href="#_footnoteref_{position()}"><xsl:value-of select="position()"/>.</a><xsl:text> </xsl:text><xsl:apply-templates /></p>
</xsl:template>


<!-- Ruler -->

<xsl:template match="ruler">
    <hr />
</xsl:template>


<!-- Tables -->

<xsl:template match="table">
    <table>
        <xsl:call-template name="block-attributes">
            <xsl:with-param name="class-list">
                <xsl:value-of select="'frame-all'"/>
            </xsl:with-param>
        </xsl:call-template>
        <xsl:apply-templates select="title" mode="table"/>
        <xsl:apply-templates select="columns" mode="table"/>
        <xsl:apply-templates select="head" mode="table"/>
        <xsl:apply-templates select="body" mode="table"/>
        <xsl:apply-templates select="foot" mode="table"/>
    </table>
</xsl:template>

<xsl:template match="columns" mode="table">
    <colgroup><xsl:apply-templates /></colgroup>
</xsl:template>

<xsl:template match="column">
    <col style="width: {@width}%"/>
</xsl:template>

<xsl:template match="head" mode="table">
    <thead><xsl:apply-templates select="tablerow"/></thead>
</xsl:template>

<xsl:template match="body" mode="table">
    <tbody><xsl:apply-templates /></tbody>
</xsl:template>

<xsl:template match="tablerow[@place='head']">
    <tr><xsl:apply-templates mode="tablehead"/></tr>
</xsl:template>

<xsl:template match="tablerow">
    <tr><xsl:apply-templates /></tr>
</xsl:template>

<xsl:template match="tablecell" mode="tablehead">
    <th scope="col" class="align-{@align} valign-{@valign}"><xsl:apply-templates /></th>
</xsl:template>

<xsl:template match="tablecell">
    <xsl:choose>
        <xsl:when test="@style='header'">
            <th scope="row" class="align-{@align} valign-{@valign}"><xsl:apply-templates /></th>
        </xsl:when>
        <xsl:otherwise>
            <td class="align-{@align} valign-{@valign}">
                <xsl:choose>
                    <xsl:when test="@style='emphasis'">
                        <p><em><xsl:apply-templates /></em></p>
                    </xsl:when>
                    <xsl:when test="@style='strong'">
                        <p><strong><xsl:apply-templates /></strong></p>
                    </xsl:when>
                    <xsl:when test="@style='monospaced'">
                        <p class="tablecell"><code><xsl:apply-templates /></code></p>
                    </xsl:when>
                    <xsl:when test="@style='literal'">
                        <pre class="tablecell"><xsl:apply-templates /></pre>
                    </xsl:when>
                    <xsl:when test="@style='verse'">
                        <div class="tablecell verse"><xsl:apply-templates /></div>
                    </xsl:when>
                    <xsl:when test="@style='quote'">
                        <p class="tablecell"><q><xsl:apply-templates /></q></p>
                    </xsl:when>
                    <!-- choose also: verse,quote,literal,header -->
                    <xsl:otherwise>
                        <p class="tablecell"><xsl:apply-templates /></p>
                    </xsl:otherwise>
                </xsl:choose>
            </td>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="title" mode="table">
    <caption><xsl:apply-templates /></caption>
</xsl:template>


<!-- Formatting -->

<xsl:template match="emphasis">
    <em><xsl:apply-templates select="@styling"/><xsl:apply-templates /></em>
</xsl:template>

<xsl:template match="strong">
    <strong><xsl:apply-templates select="@styling"/><xsl:apply-templates /></strong>
</xsl:template>

<xsl:template match="monospaced">
    <code><xsl:apply-templates select="@styling"/><xsl:apply-templates /></code>
</xsl:template>

<xsl:template match="singlequoted">
    <xsl:text>‘</xsl:text><xsl:apply-templates /><xsl:text>’</xsl:text>
</xsl:template>

<xsl:template match="doublequoted">
    <xsl:text>“</xsl:text><xsl:apply-templates /><xsl:text>”</xsl:text>
</xsl:template>

<xsl:template match="superscript">
    <sup><xsl:apply-templates /></sup>
</xsl:template>

<xsl:template match="subscript">
    <sub><xsl:apply-templates /></sub>
</xsl:template>

<xsl:template match="@styling">
    <xsl:attribute name="class"><xsl:value-of select="."/></xsl:attribute>
</xsl:template>

<xsl:template match="br">
    <br />
</xsl:template>

<xsl:template match="*">
</xsl:template>

</xsl:stylesheet>
