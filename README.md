# AsciiProc version 0.1.

An AsciiDoc processor written in PHP.

## Requirements

PHP 7.0+ with XSLT enabled.

## Current limitations

* Support for tables is incomplete.

## Syntax highlighting for code blocks

Syntax highlighting is provided for C++, CSS, HTML, JavaScript, PHP, Python, Ruby, Rust, SCSS, Twig and XML. More languages will be added.

## How to use

### An example
```php
require "asciiproc/asciiproc.php";

$doc = new APDocument();
$doc->load("source_doc.adoc");
// $doc now contains an XML representation of the source document. To
// obtain a useful output it is necessary to transform the XML using XSLT.
$xform = new APTransformer();
$xform->load("stylesheet.xsl");
$result = $xform->transformToXML($doc); //string output
echo $result;
```

### Class APDocument public methods
```php
load (string $file_path): bool

loadText (string $source_text): bool
```
### Class APTransformer public methods
```php
importStylesheet (DOMDocument $stylesheet): bool

importStylesheetFromFile (string $file_path): bool

transformToXml (DOMDocument $xml_document): string

transformToDoc (DOMDocument $xml_document): DOMDocument
```

### Convenience functions
```php
asciiproc_string_to_html5_article (string $source_text): string

asciiproc_file_to_html5_article (string $file_path): string

asciiproc_string_to_docbook5_article (string $source_text): string

asciiproc_file_to_docbook5_article (string $file_path): string

asciiproc_string_to_docbook5_book (string $source_text): string

asciiproc_file_to_docbook5_book (string $file_path): string
```
### Error reporting

Error messages are written to the system error log.
