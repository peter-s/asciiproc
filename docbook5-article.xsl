<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
    xmlns="http://docbook.org/ns/docbook"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml"
    omit-xml-declaration="no"
    encoding="UTF-8"
    indent="yes"/>

<xsl:template match="/">
    <xsl:apply-templates select="//section[@level='0'][1]"/>
</xsl:template>


<!-- Document template -->

<xsl:template match="section[@level='0'][1]">
    <article version="5.1">
        <xsl:apply-templates select="head" mode="info"/>
        <xsl:apply-templates select="body"/>
    </article>
</xsl:template>

<!--<xsl:template match="section[@level='0']/head">-->
<xsl:template match="head" mode="info">
    <info>
        <xsl:apply-templates />
    </info>
</xsl:template>

<xsl:template match="section[@level='0']/body">
    <xsl:apply-templates />
</xsl:template>


<!-- Section -->

<xsl:template match="section[@style = 'preface']">
    <preface>
        <xsl:apply-templates select="head"/>
        <xsl:apply-templates select="body"/>
    </preface>
</xsl:template>

<xsl:template match="section">
    <section>
        <xsl:call-template name="block-id"/>
        <xsl:apply-templates select="head"/>
        <xsl:apply-templates select="body"/>
    </section>
</xsl:template>

<!-- Section title -->
<xsl:template match="title">
    <title><xsl:apply-templates /></title>
</xsl:template>


<!-- Block ID attribute -->
<xsl:template name="block-id">
    <xsl:if test="@id">
        <xsl:attribute name="xml:id"><xsl:value-of select="./@id"/></xsl:attribute>
    </xsl:if>
</xsl:template>

<xsl:template match="head">
    <xsl:apply-templates select="title"/>
</xsl:template>

<xsl:template match="body">
    <xsl:apply-templates />
</xsl:template>


<!-- Blocks -->

<xsl:template match="listingblock">
    <xsl:choose>
        <xsl:when test="title">
            <formalpara>
                <xsl:call-template name="block-id"/>
                <xsl:apply-templates select="title"/>
                <para><xsl:call-template name="listing-content"/></para>
            </formalpara>
        </xsl:when>
        <xsl:otherwise>
            <para>
                <xsl:call-template name="block-id"/>
                <xsl:call-template name="listing-content"/>
            </para>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="listing-content">
    <screen>
        <xsl:if test="@language"><xsl:attribute name="language"><xsl:value-of select="@language"/></xsl:attribute></xsl:if>
        <xsl:copy-of select="body/text()"/>
    </screen>
</xsl:template>

<xsl:template match="admonitionblock">
    <xsl:element name="{@name}">
        <xsl:call-template name="block-id"/>
        <xsl:apply-templates select="title"/>
        <xsl:apply-templates select="body"/>
    </xsl:element>
</xsl:template>

<xsl:template match="quoteblock">
    <blockquote>
        <xsl:apply-templates select="title"/>
        <xsl:apply-templates select="body"/>
        <xsl:apply-templates select="attribution"/>
    </blockquote>
</xsl:template>

<xsl:template match="verseblock">
    <blockquote>
        <xsl:apply-templates select="title"/>
        <xsl:apply-templates select="body" mode="literal"/>
        <xsl:apply-templates select="attribution"/>
    </blockquote>
</xsl:template>

<xsl:template match="attribution">
    <attribution><xsl:apply-templates /></attribution>
</xsl:template>

<xsl:template match="citetitle">
    <citetitle><xsl:copy-of select="text()"/></citetitle>
</xsl:template>

<xsl:template match="body" mode="literal">
    <literallayout>
        <xsl:apply-templates />
    </literallayout>
</xsl:template>

<xsl:template match="body">
    <xsl:apply-templates />
</xsl:template>


<!-- Paragraphs -->

<xsl:template match="simpara">
    <simpara><xsl:apply-templates /></simpara>
</xsl:template>

<xsl:template match="para">
    <xsl:choose>
        <xsl:when test="title">
            <formalpara>
                <xsl:apply-templates select="title"/>
                <simpara><xsl:apply-templates select="body"/></simpara>
            </formalpara>
        </xsl:when>
        <xsl:otherwise>
            <simpara><xsl:apply-templates select="body"/></simpara>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<!-- Block title -->

<xsl:template match="title">
    <title><xsl:apply-templates /></title>
</xsl:template>


<!-- Lists -->

<!-- Bulleted list -->
<xsl:template match="bulletedlist">
    <itemizedlist>
        <xsl:call-template name="block-id"/>
        <xsl:apply-templates />
    </itemizedlist>
</xsl:template>


<!-- Numbered list -->
<xsl:template match="numberedlist">
    <orderedlist numeration="{@style}">
        <xsl:call-template name="block-id"/>
        <xsl:apply-templates />
    </orderedlist>
</xsl:template>

<xsl:template match="listitem">
    <listitem><xsl:apply-templates /></listitem>
</xsl:template>


<!-- Labeled list -->
<xsl:template match="labeledlist">
    <variablelist>
        <xsl:apply-templates select="title"/>
        <xsl:apply-templates select="body" mode="labeledlist"/>
    </variablelist>
</xsl:template>

<xsl:template match="body" mode="labeledlist">
    <varlistentry><xsl:apply-templates mode="labeledlist"/></varlistentry>
</xsl:template>

<xsl:template match="label" mode="labeledlist">
    <label><xsl:apply-templates /></label>
</xsl:template>

<xsl:template match="listitem" mode="labeledlist">
    <listitem><xsl:apply-templates/></listitem>
</xsl:template>


<!-- Formatting -->

<xsl:template match="emphasis">
    <emphasis><xsl:apply-templates /></emphasis>
</xsl:template>

<xsl:template match="strong">
    <emphasis role="strong"><xsl:apply-templates /></emphasis>
</xsl:template>

<xsl:template match="monospaced">
    <code><xsl:apply-templates /></code>
</xsl:template>

<xsl:template match="singlequoted">
    <xsl:text>‘</xsl:text><xsl:apply-templates /><xsl:text>’</xsl:text>
</xsl:template>

<xsl:template match="doublequoted">
    <xsl:text>“</xsl:text><xsl:apply-templates /><xsl:text>”</xsl:text>
</xsl:template>

<xsl:template match="superscript">
    <superscript><xsl:apply-templates /></superscript>
</xsl:template>

<xsl:template match="subscript">
    <subscript><xsl:apply-templates /></subscript>
</xsl:template>

<xsl:template match="br">
    <xsl:processing-instruction name="asciidoc-br"/>
</xsl:template>


<!-- Match-all -->

<xsl:template match="*">
</xsl:template>

</xsl:stylesheet>
