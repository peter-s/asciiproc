<?php

define('AD_HIGHLIGHTERS', __DIR__ . '/highlighters.txt');

class Highlighter
{
    private static $highlighters = [];

    private $configuration;

    private $keywordSets;

    public $regexps;

    function setLanguage(string $language = null)
    {
        if (empty(self::$highlighters)) {
            $highlighters_text = file(AD_HIGHLIGHTERS);
            if (!empty($highlighters_text)) {
                foreach ($highlighters_text as $line) {
                    if ($line) {
                        $parts = str_getcsv($line);
                        $language_name = strtolower($parts[0]);
                        foreach ($parts as $part) {
                            self::$highlighters[strtolower($part)] = $language_name;
                        }
                    }
                }
            }
        }
        $language = self::$highlighters[strtolower($language)] ?? null;
        if (!$language) return false;

        $file = __DIR__ . "/highlight-$language.ini";
        if (!is_file($file)) return false;

        $config = parse_ini_file($file, true);
        $keyword_sets = $config['keywords'] ?? null;
        if (is_array($keyword_sets)) {
            $this->keywordSets = [];
            foreach ($keyword_sets as $keyword_set) {
                $this->keywordSets[] = preg_split("/\s+/", trim($keyword_set));
            }
            unset($config['keywords']);
        }
        /*if (empty($this->keywordSets)) {
            var_dump($keyword_sets); die;
        }*/
        $this->configuration = $config;
        #print_r($this->configuration); die;
        $this->regexps = [];
        foreach ($config['highlighting'] as $list_name => $list) {
            $exp_parts = [];
            foreach (str_getcsv($list) as $part_name) {
                $exp_part = $config['regexp_partials'][$part_name] ?? null;
                $exp_parts[] = "(?<$part_name>$exp_part)";
            }
            $this->regexps[$list_name] = "~" . implode('|', $exp_parts) . "~s";
        }
    }

    function highlight(string $text = null)
    {
        if ($text and !empty($this->regexps)) {
            $text = str_replace(['&', '<', '>'], ['&amp;', '&lt;', '&gt;'], $text);
            if (is_string($this->regexps['primary'])) {
                $text = $this->processContent($text, $this->regexps['primary']);
            }
        }
        return $text;
    }

    function processContent(string $text, string $regexp)
    {
        $text = preg_replace_callback(
            $regexp,
            function ($matches) {
                $name = null;
                foreach ($matches as $key => $value) {
                    $found = false;
                    if (is_string($key) and !empty($value)) {
                        $name = $key;
                        $tag_content = $value;
                    }
                }
                if ($name) {
                    if ($name == 'keyword') {
                        if (!empty($this->keywordSets)) {
                            foreach ($this->keywordSets as $set_num => $keyword_set) {
                                if (in_array($tag_content, $keyword_set)) {
                                    $name .= strval($set_num + 1);
                                    #$name .= strval($set_num);
                                    break;
                                }
                            }
                        }
                    } elseif (isset($this->regexps[$name])) {
                        $tag_content = $this->processContent($tag_content, $this->regexps[$name]);
                    }
                    return "<span class=\"$name\">$tag_content</span>";
                }
            },
            $text
        );
        return $text;
    }
}
