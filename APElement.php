<?php

class APElement extends DOMElement
{
    /**
     * @var array various values
     */
    public $values;

    /**
     * Getter.
     */
    function __get(string $name)
    {
        $is = APDocument::$elementInfo[$this->nodeName]['is'] ?? null;
        if ($name == 'is') {
            return $is;
        }
        if (in_array($name, ['head', 'body', 'foot'])) {
            if ($is == 'titled-block') {
                $children = $this->childNodes;
                if (!empty($children)) {
                    foreach ($children as $child_node) {
                        if ($child_node->nodeName == $name) {
                            $target_node = $child_node;
                        }
                    }
                }
                if (!isset($target_node)) {
                    $target_node = $this->appendElement($name);
                }
                return $target_node;
            } else {
                return $this;
            }
        }
    }

    /**
     * Append child element to DOM.
     *
     * @param DOMNode $element
     */
    function appendChild(DOMNode $element)
    {
        parent::appendChild($element);
        if (method_exists($element, 'onAppend')) {
            $element->onAppend();
        }
        return $element;
    }

    function appendElement(string $element_name, $content = null, array $values = [])
    {
        $element = $this->ownerDocument->createElement($element_name, $content, $values);
        $this->appendChild($element);
        return $element;
    }

    function bodyAppendElement(string $element_name, $content = null, array $values = [])
    {
        $element = $this->ownerDocument->createElement($element_name, $content, $values);
        $this->body->appendChild($element);
        return $element;
    }

    /**
     * Set element attributes.
     *
     * @param array $values array of names and values
     */
    function setAttributes(array $values = null)
    {
        if (!$values) return;
        if (isset(APDocument::$elementInfo[$this->nodeName]['attributes'])) {
            $attr_names = str_getcsv(APDocument::$elementInfo[$this->nodeName]['attributes']);
            foreach ($attr_names as $name) {
                if (isset($values[$name])) {
                    $this->setAttribute($name, $values[$name]);
                }
            }
        }
    }

    /**
     * Called when the element is appended to the document.
     */
    function onAppend()
    {
        $this->setAttributes($this->values);
    }
/*
    function headAppend(APElement $element)
    {
        if (!isset($this->head)) {
            $head_element = $this->ownerDocument->createElement('head');
            if (isset($this->firstChild)) {
                $this->head = $this->insertBefore($this->firstChild, $head_element);
            } else {
                $this->head = $this->appendChild($head_element);
            }
        }
        $this->head->appendChild($element);
    }
*/
    /**
     * Append element or fragment to body element within this element.
     *
     * @param DOMNode $node element to append
     */
    function bodyAppend(DOMNode $node)
    {
       $this->body->appendChild($node);
        return $node;
    }
}
