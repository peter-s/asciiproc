<?php

require 'APReader.php';
require 'APElement.php';
require 'APListElement.php';
require 'APTableElement';
require 'highlighter/highlighter.php';

// Attribute refs used for substitutions
//define('EXP_ATTRIBUTE_REFS', "\{((?<names>\w+)(?<operator>\=|\?|\!|\#|\%)(?<value>.*?))?((?<names>\w+)@)?(?<name>\w+)?\}");
define('EXP_ATTRIBUTE_REFS', "/\{(?<name>\w.*?)\}/s");

define('EXP_AUTHOR', "^(?<firstname>[A-Za-z'_]+)\s+(?<middlename>[A-Za-z'_]+)(\s+(?<lastname>[A-Za-z'_]+))?(\s+<(?<email>.+?)>)?");

define('EXP_REVISION', "((?<revnumber>[A-Za-z0-9-.]+)\s*,)(?<revdate>.*?)((?<colon>\:)\s*(?<revremark>.*))");

define('EXP_ROMAN_NUMERAL_PARTS',
    "/^(CM|DC{0,3}|CD|C{1,3}|)(XC|LX{0,3}|XL|X{1,3}|)(IX|VI{0,3}|IV|I{1,3}|)$/"
);


/**
 * Class APDocument parses an AsciiDoc document into an XML tree.
 */
class APDocument extends DOMDocument
{
    static $romanToDecimal = [
        'I' => 1,
        'II' => 2,
        'III' => 3,
        'IV' => 4,
        'V' => 5,
        'VI' => 6,
        'VII' => 7,
        'VIII' => 8,
        'IX' => 9,
        'X' => 10,
        'XX' => 20,
        'XXX' => 30,
        'XL' => 40,
        'L' => 50,
        'LX' => 60,
        'LXX' => 70,
        'LXXX' => 80,
        'XC' => 90,
        'C' => 100,
        'CC' => 200,
        'CCC' => 300,
        'CD' => 400,
        'D' => 500,
        'DC' => 600,
        'DCC' => 700,
        'DCCC' => 800,
        'CM' => 900
    ];

    static $elementInfo = [
        'admonitionblock' => [
            'is' => 'titled-block',
            'attributes' => 'id,role'
        ],
        'asciiproc-document' => [
            'is' => 'root'
        ],
        'attribution' => [
            'is' => 'raw-text'
        ],
        'author' => [
            'is' => 'raw-text'
        ],
        'bulletedlist' => [
            'is' => 'titled-block',
            'attributes' => 'id,role,style,what'
        ],
        'calloutlist' => [
            'is' => 'titled-block',
            'attributes' => 'id,role,style,what'
        ],
        'citetitle' => [
            'is' => 'formatted-text'
        ],
        'column' => [
            'is' => '',
            'attributes' => 'width'
        ],
        'comment' => [
            'is' => 'raw-text'
        ],
        'commentblock' => [
            'is' => 'raw-text',
        ],
        'email' => [
            'is' => 'raw-text'
        ],
        'exampleblock' => [
            'is' => 'titled-block',
            'attributes' => 'id,role'
        ],
        'footnote' => [
            'attributes' => 'target'
        ],
        'footnote2' => [
            'attributes' => 'target'
        ],
        'image' => [
            'is' => 'macro',
            'attributes' => 'align,alt,float,height,id,link,role,scale,scaledwidth,target,title,width'
        ],
        'indexterm' => [
        ],
        'label' => [
            'is' => 'formatted-text'
        ],
        'labeledlist' => [
            'is' => 'titled-block',
            'attributes' => 'id,role,style,what'
        ],
        'link' => [
            'attributes' => 'name,id,target,title'
        ],
        'listingblock' => [
            'is' => 'titled-block',
            'attributes' => 'id,language,linenos,role'
        ],
        'listitem' => [
            'is' => 'list-item',
            'children-disallowed' => 'section'
        ],
        'literalblock' => [
            'is' => 'titled-block',
            'attributes' => 'id,role'
        ],
        'numberedlist' => [
            'is' => 'titled-block',
            'attributes' => 'id,role,style,start,what'
        ],
        'openblock' => [
            'is' => 'titled-block',
            'attributes' => 'id,role,style'
        ],
        'para' => [
            'is' => 'titled-block',
            'attributes' => 'id,role,style'
        ],
        'quoteblock' => [
            'is' => 'titled-block',
            'attributes' => 'id,role',
            'childen-disallowed' => 'section,sidebarblock'
        ],
        'revdate' => [
            'is' => 'raw-text'
        ],
        'revnumber' => [
            'is' => 'raw-text'
        ],
        'revremark' => [
            'is' => 'formatted-text'
        ],
        'ruler' => [
            'is' => 'ruler'
        ],
        'section' => [
            'is' => 'titled-block',
            'attributes' => 'id,level,style,role'
        ],
        'sidebarblock' => [
            'is' => 'titled-block',
            'attributes' => 'id,role,style',
            'childen-disallowed' => 'section,sidebarblock'
        ],
        'simpara' => [
            'is' => 'formatted-text'
        ],
        'table' => [
            'is' => 'titled-block',
            'attributes' => 'id,role,style'
        ],
        'tablecell' => [
            'is' => 'formatted-text',
            'attributes' => 'style,align,valign'
        ],
        'title' => [
            'is' => 'formatted-text'
        ],
        'verseblock' => [
            'is' => 'titled-block',
            'attributes' => 'id,role',
            'childen-disallowed' => 'section,sidebarblock'
        ],
        'xref' => [
            'attributes' => 'target,alt'
        ]
    ];

    static $defaultAttributes = [
        'doctitle' => '',
        'idprefix' => '_',
        'idseparator' => '_',
        'sectids' => null,
        'toc' => 'auto',
        'leveloffset' => 0,
    ];

    static $substitutions = [
        'header' => 'specialcharacters,attributes',
        'none' => '',
        'normal' => 'specialcharacters,quotes,attributes,replacements,macros,replacements2',
        'pass' => '',
        'verbatim' => 'specialcharacters'
    ];

    /**
     * @var array the configuration loaded from asciiproc.ini
     */
    public $configuration;

    /**
     * @var string the name of the file currently being parsed
     */
    private $filename;

    /**
     * @var APReader instance of the APReader class
     */
    private $reader;

    /**
     * @var array
     */
    public $globalAttributes = [];

    /**
     * @var DOMElement the current base element, usually the document root element
     */
    private $baseElement;

    /**
     * @var DOMElement the current section element
     */
    private $sectionElement;

    /**
     * @var array holds the list elements in the current list block
     */
    private $lists;

    /**
     * @var bool true if list continuation is taking place
     */
    private $listContinuation;

    /**
     * @var array used for processing quote spans
     */
    private $quotes = [];

    /**
     * @var int
     *  Holds the number of tables created thus far.
     */
    private $tableCount = 0;

    /**
     * @var array syntax highlighter info
     */
    private $highlighter_info = [];

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
        $this->registerNodeClass('DOMElement', 'APElement');

        // Parse configuration file.
        $config = parse_ini_file(ASCIIPROC_CONFIG_FILE, true, INI_SCANNER_RAW);
        reset($config);
        do {
            $values = current($config);
            if (isset($values['base-def'])) {
                $values = array_merge($config[$values['base-def']], $values);
                unset($values['base-def']);
            }
            $config[key($config)] = $values;
        } while (next($config));

        $this->configuration = $config;

        foreach($config['attributes'] as $name => $value) {
            $enabled = true;
            if ($name[0] == '?') {
                $name = trim($name, '?');
                $enabled = false;
            }
            $this->globalAttributes[$name] = ['enabled' => $enabled, 'value' => $value];
        }

        // Set up text formatting array.
        $def_num = 0;
        foreach ($config['quotes'] as $delimiters => $tag) {
            $delimiters = str_getcsv($delimiters);
            $tag = str_getcsv($tag, '.');
            $entry = array(
                'start_delimiter' => $delimiters[0],
                'end_delimiter' => isset($delimiters[1]) ? $delimiters[1] : $delimiters[0],
                'tag_name' => $tag[0],
                'flags' => isset($tag[1]) ? $tag[1] : null
            );
            $start_delimiter = preg_quote(rawurldecode($entry['start_delimiter']));
            $end_delimiter = preg_quote(rawurldecode($entry['end_delimiter']));
            if (strpos($entry['flags'], 'u') !== false) {
                $entry['exp'] = "(?<!\\\\)$start_delimiter(?<d$def_num>.+?)$end_delimiter";
            } else {
                $entry['exp'] = "(?J)(?<![A-Za-z0-9\\\\])(\[(?<attributes>[^\]]+)\])?$start_delimiter(?<d$def_num>.+?)$end_delimiter(?![A-Za-z0-9])";
            }
            $this->quotes[] = $entry;
            $def_num++;
        }
    }

    /**
     * Overrides DOMDocument load method.
     *
     * @param string $file_path Path to AsciiDoc file
     */
    public function load($file_path, $options = null)
    {
        if (is_file($file_path)) {
            $text = file_get_contents($file_path);
            $filename = basename($file_path);
            $this->loadText($text, $filename);
            return true;
        } else {
            $this->writeToLog('F', "file '$file_path' not found", false);
            return false;
        }
    }

    /**
     * Convert text input into XML.
     *
     * @param string $text
     */
    public function loadText(string $text, string $filename )
    {
        $this->loadXML('<asciiproc-document></asciiproc-document>');
        $this->formatOutput = true;
        $this->process($this->documentElement, $text, 0, $filename);

        // Add global attributes to XML tree.
        $attr_names = ['title'];
        $toc = $this->getGlobalAttr('toc');
        if ($toc) {
            $attr_names = array_merge($attr_names, ['toc', 'toclevels', 'toc-title']);
        }
        $element_before = $this->documentElement->firstChild;
        foreach ($attr_names as $name) {
            $element = $this->createElement($name);
            $element->textContent = strval($this->getGlobalAttr($name));
            $this->documentElement->insertBefore($element, $element_before);
        }

        return true;
    }


    /**
     * Main processing loop.
     *
     * @param DOMNode $base_element
     * @param string  $text
     *  Text to be processed.
     */
    function process(DOMNode $base_element = null, string $text, int $base_line_num = 0, string $filename = null, array $values = null)
    {
        $old_filename = $this->filename;
        $old_reader = $this->reader;
        $old_base_element = $this->baseElement;
        $old_section_element = $this->sectionElement;
        $old_lists = $this->lists;
        $old_list_continuation = $this->listContinuation;

        $this->filename = $filename ? $filename : $this->filename;
        $this->reader = new APReader();
        $this->baseElement = $base_element;
        $this->sectionElement = $base_element;
        $this->lists = [];
        $this->listContinuation = false;

        $this->reader->loadDoc($text, $base_line_num);

        // Main process.
        while ($this->reader->refNextLine() !== false) {
            if ($this->reader->currentLineIsBlank) continue;
            $result = $this->getMatchResultFor('titledef|blockmacro|listdef|blockdef|tabledef|attributeentry|attributelist|blocktitle|paradef-(?!default)|paradef-default');
            if (is_array($result)) {
                $this->processMatchResult($result);
            }
        }

        $this->filename = $old_filename;
        $this->reader = $old_reader;
        $this->baseElement = $old_base_element;
        $this->sectionElement = $old_section_element;
        $this->lists = $old_lists;
        $this->listContinuation = $old_list_continuation;
    }

    /**
     * Find match result for an item definition or set of definitions
     *
     * @param string $groups
     */
    function getMatchResultFor(string $groups)
    {
        $def_keys = array_keys($this->configuration);
        $keys_found = [];
        foreach (explode('|', $groups) as $exp) {
            $keys_found = array_merge($keys_found, preg_grep("/^$exp/", $def_keys));
        }

        foreach ($keys_found as $def) {
            $values = $this->configuration[$def];
            if (!isset($values['delimiter'])) continue;
            $delimiter = $values['delimiter'];

            if ($this->reader->currentLineMatch($delimiter)) {
                $match = $this->reader->lastMatch;
                if (!isset($values['underlines'])) {
                    break;
                }
                // Check for underline.
                $underlines = $values['underlines'];
                $length = strlen($match['full_match']);
                $min_length = ($length > 1) ? $length - 1 : $length;
                $max_length = $length + 1;
                for ($level = 0; $level < strlen($underlines); $level++) {
                    $underline_char = $underlines[$level];
                    $exp = '^' . preg_quote($underlines[$level]) . '{' . strval($min_length) . ',' . strval($max_length) . '}$';
                    if ($this->reader->nextLineMatch($exp)) {
                        $this->reader->refNextLine();
                        $values['level'] = strval($level);
                        break 2;
                    }
                }
                $match = null;
            }
        }

        if (!isset($match)) return false;

        $values['def'] = $def;
        $values['def_group'] = explode('-', $def)[0];

        $values = array_merge($values, $match);
        $render_xml = false;
        switch ($values['def_group']) {
            case 'attributeentry':
            if (isset($values['attrname2'])) {
                // Set configuration value
                // Format: :<section name>.<entry name>: <value>
            } else {
                $this->setGlobalAttr($values['attrname'], $values['attrvalue'] ?? null);
                #echo $values['attrvalue'].'<br>';
            }
            return true;
            #break;
            case 'attributelist':
            #printout($values);
            if (isset($values['id'])) {
                $this->blockAttributes['id'] = $values['id'];
                if (isset($values['reftext'])) {
                    $this->blockAttributes['reftext'] = $values['reftext'];
                }
            } else {
                // Dissect attribute list
                $this->parseAttributeList($values['attrlist'], $this->blockAttributes, $this->posAttributes);
            }
            return true;
            #break;
            case 'blocktitle':
            $this->blockAttributes['title'] = $values['title'];
            break;
            case 'titledef':
            $offset = $this->getGlobalAttr('leveloffset');
            #echo "Off: $offset ";
            $values['leveloffset'] = isset($offset) ? $offset : 0;
            case 'blockmacro':
            case 'blockdef':
            case 'listdef':
            case 'paradef':
            case 'tabledef':
            $render_xml = true;
            break;
        }

        if (!$render_xml) return;

        // Set style.
        if (isset($this->blockAttributes)) {
            $block_attributes = $this->blockAttributes;
            $values = array_merge($block_attributes, $values);
        }
        if (isset($match['style'])) {
            $values['style'] = $match['style'];
        } elseif (isset($values['posattrs']) && strpos($values['posattrs'], 'style') === 0) {
            if (isset($this->posAttributes[0])) {
                $values['style'] = $this->posAttributes[0];
            }
        } elseif (isset($this->blockAttributes['style'])) {
            $values['style'] = $this->blockAttributes['style'];
        }

        if (isset($values['style'])) {
            $style = $values['style'];
            if (isset($values["$style-style"]) and is_array($values["$style-style"])) {
                $values = array_merge($values, $values["$style-style"]);
            } elseif (isset($this->configuration["style-$style"])) {
                $values = array_merge($values, $this->configuration["style-$style"]);
            }
        }

        $id = $this->getAttributeValue('id');
        if ($id) $values['id'] = $id;
        $title = $this->getAttributeValue('title');
        if ($title) $values['title'] = $title;

        if (isset($values['posattrs'])) {
            foreach (str_getcsv($values['posattrs']) as $pos => $name) {
                if (!isset($values[$name])) {
                    if (isset($this->posAttributes[$pos])) {
                        $values[$name] = $this->posAttributes[$pos];
                    }
                }
            }
        }

        if (isset($values['render'])) {
            $render_split = explode('/', $values['render']);
            $values['render'] = $render_split[0];
            if (isset($render_split[1])) {
                $values['name'] = $render_split[1];
            }
        }
        $this->posAttributes = [];
        $this->blockAttributes = [];
        return $values;
    }


    function getAttributeValue(string $name, string $posattrs = null)
    {
        if (isset($posattrs)) {
            $posattr_keys = str_getcsv($posattrs);
            $pos = array_search($name, $posattr_keys);
            if ($pos !== false && isset($this->posAttributes[$pos])) {
                return $this->posAttributes[$pos];
            }
        }
        if (isset($this->blockAttributes[$name])) {
            return $this->blockAttributes[$name];
        }
    }

    /**
     * Find the element that the next block is to be atached to.
     *
     * @param bool $allow_list_element
     *  If true then block can be attached to a list.
     */
    function getAttachmentElement(bool $allow_list_element = false)
    {
        if (!isset($this->sectionElement)) {
            $this->sectionElement = $this->baseElement;
        }
        if ($this->listContinuation) {
            $this->listContinuation = false;
            return end($this->lists);
        } elseif ($allow_list_element and $this->currentBlockIsList()) {
            if (!empty($this->lists)) {
                return end($this->lists);
            } else {
                return $this->sectionElement;
            }
        } else {
            if ($this->baseElement === $this->documentElement) {
                // If current section is level 0 and has a title then create level 1 section
                $current_section = $this->sectionElement;
                if ($current_section === $this->documentElement) {
                    $this->sectionElement = $current_section->appendElement('section', null, ['level' => '0']);
                } elseif ($current_section->nodeName == 'section' and $current_section->getAttribute('level') == '0') {
                    if (isset($current_section->firstChild) and $current_section->firstChild->nodeName == 'head') {
                        $this->sectionElement = $current_section->body->appendElement('section', null, ['level' => '1', 'style' => 'preface']);
                    }
                }
            }
            return $this->sectionElement;
        }
    }


    /**
     * Process match result.
     *
     * @param array $values
     *  Various values.
     */
    function processMatchResult(array $values = null)
    {
        $def_group = explode('-', $values['def'])[0];
        $function_name = 'process' . ucfirst($def_group);
        $this->$function_name($values);
    }

    // Block processors.

    /**
     * Render section title.
     *
     * @param array $values various values
     */
    function processTitledef(array $values)
    {
        $add_toc = false;
        $base_element = $this->baseElement;
        $title = $values['title'];
        $def = $values['def'];
        $level = intval($values['level']) + intval($values['leveloffset']);

        if ($level == 0) {
            // Look for meta data underneath.
            if ($this->getGlobalAttr('title') == null) {
                $this->setGlobalAttr('title', $title);
            }
            if ($this->reader->nextLineIsBlank) {
                $this->reader->refNextLine();
            }
            if (!$this->reader->nextLineIsBlank) {
                if ($this->reader->nextLineMatch(EXP_AUTHOR)) {
                    $values = $this->reader->lastMatch;
                    $firstname = $values['firstname'];
                    $author = $firstname;
                    if (isset($values['lastname'])) {
                        $middlename = $values['middlename'];
                        $lastname = $values['lastname'];
                        $author .= ' ' . $middlename;
                    } else {
                        $middlename = '';
                        $lastname = $values['middlename'];
                    }
                    $author .= ' ' . $lastname;
                    $email = $values['email'] ?? '';
                    $this->setGlobalAttr('authored', '');
                    $this->setGlobalAttr('author', $author);
                    $this->setGlobalAttr('firstname', $firstname);
                    $this->setGlobalAttr('middlename', $middlename);
                    $this->setGlobalAttr('lastname', $lastname);
                    $this->setGlobalAttr('email', $email);
                }
            }
            $this->reader->refNextLine();
            if (isset($author) and !$this->reader->nextLineIsBlank) {
                if ($this->reader->nextLineMatch(EXP_REVISION)) {
                    $values = $this->reader->lastMatch;
                    $this->reader->refNextLine();
                    if (isset($values['revnumber'])) {
                        $this->setGlobalAttr('revnumber', $values['revnumber']);
                    }
                    if (isset($values['revdate'])) {
                        $this->setGlobalAttr('revdate', trim($values['revdate']));
                    }
                    if (isset($values['colon'])) {
                        $revremark = isset($values['revremark']) ? $values['revremark'] : '';
                        while (!$this->reader->nextLineIsBlank) {
                            $this->reader->refNextLine();
                            $revremark .= $this->reader->currentLineText;
                        }
                        $this->setGlobalAttr('revremark', $revremark);
                    }
                }
            }
            $add_toc = in_array($this->getGlobalAttr('toc'), ['auto', 'left', 'right']);
        }

        if ($this->listContinuation) {
        return;
            $this->writeToLog('F', "cannot attach section to list item"); die;
        }

        /*if ($level == 0) {
            if ($base_element->hasChildNodes()) {
                $this->writeToLog('E', "article title should come first in document");
            }
        }*/

        // Add section numbering if required.
        if ($this->globalAttrEnabled('sectnums') or $this->globalAttrEnabled('numbering')) {
            $current_level = intval($values['level']);
            $sectnum_levels = $this->getGlobalAttr('sectnumlevels') ?? 100;
            $this->sectionCounters[$current_level]++;
            $this->sectionCounters[$current_level + 1] = 0;
            $sectnum_string = '';
            if ($current_level <= $sectnum_levels) {
                for ($i = 1; $i <= $current_level; $i++) {
                    $sectnum_string .= strval($this->sectionCounters[$i]) . '.';
                }
            }
            $values['title'] = $sectnum_string . ' ' . $values['title'];
        }

        // Find attachment element;
        #$title_element = $this->createElement('title', $title);
        $ancestor_element = $this->sectionElement;
        while ($ancestor_element !== $base_element) {
            if ($ancestor_element->nodeName == 'section') {
                $ancestor_level = intval($ancestor_element->getAttribute('level'));
                if ($ancestor_level < $level) break;
            }
            $ancestor_element = $ancestor_element->parentNode;
        }
        $section = $ancestor_element->body->appendElement('section', null, $values);
        $section->setAttribute('level', strval($level));
        #$section->head->appendChild($title_element);
        $this->sectionElement = $section;
        if ($level == 0) {
            foreach (['title', 'author', 'email', 'revnumber', 'revdate', 'revremark'] as $name) {
                $value = $this->getGlobalAttr($name);
                if (!is_null($value)) {
                    $section->head->appendElement($name, $value);
                }
            }
            #$this->sectionElement = $base_element;
        }

        if (!isset($values['id'])) {
            $idprefix = $this->getGlobalAttr('idprefix');
            $idseparator = $this->getGlobalAttr('idseparator');
            $id = strtolower($title);
            $id = preg_replace("/\s+_|_\s+|\s+|\*|\+|\=/", $idseparator, $id);
            $id = $idprefix . preg_replace("/_{2}/", $idseparator, $id);
        } else {
            $id = $values['id'];
        }
        $section->setAttribute('id', trim($id));

        if ($add_toc) {
            $section->body->appendElement('render-toc');
        }
        return true;
    }


    /**
     * Process block macro.
     *
     * @param array $values
     */
    function processBlockmacro(array $values)
    {
        $attachment_element = $this->getAttachmentElement();
        $what = explode('-', $values['def'])[1];
        $name = $values['name'] ?? null;
        switch ($what) {
            case 'default':
            if (isset($values['attrlist'])) {
                $this->parseAttributeList($values['attrlist'], $block_attrs, $pos_attrs);
                if (isset($pos_attrs[0])) {
                    $values['alt'] = $pos_attrs[0];
                }
                $values = array_merge($values, $block_attrs);
            }
            if ($name == 'include') {
                $file_path = $values['target'];
                if ($content = $this->getFileContents($file_path)) {
                   $this->process($this->sectionElement, $content, 0, $file_path);
                }
            } elseif ($name == 'toc') {
                $attachment_element->body->appendElement('render-toc');
            } else {
                $element = $attachment_element->body->appendElement($values['name']);
                $element->setAttributes($values);
            }
            break;
            case 'ruler':
            $element = $attachment_element->body->appendElement('ruler');
            break;
            case 'pagebreak':
            $element = $attachment_element->body->appendElement('pagebreak');
            break;
            case 'comment':
            $attachment_element->body->appendElement('comment', $values['passtext']);
            break;
        }
    }


    /**
     * Render list.
     * @param array $values Various values
     */
    function processListdef(array $values)
    {
        $def = $values['def'];
        $render = $values['render'];
        $values['what'] = $render . ':' . $values['def'];
        $what = $values['what'];
        $text = $values['text'] ?? null;

        $instance = null;
        if ($this->currentBlockIsList()) {
            foreach ($this->lists as $index => $element) {
                if ($element->getAttribute('what') == $what) {
                    $instance = $element;
                    array_splice($this->lists, $index + 1);
                    break;
                }
            }
        } else {
            $this->lists = [];
        }
        if (!$instance) {
            $parent_element = $this->getAttachmentElement(true);
        }

        $item_index = null;
        if (isset($values['index'])) {
            $index = $values['index'];
            $end_char = substr($index, -1, 1);
            $index = substr($index, 0, strlen($index) - 1);
            if ($end_char == '.' or $end_char == '>') {
                if (is_numeric($index)) {
                    $item_index = intval($index);
                } else {
                    $item_index = ord(strtoupper($index)) - 32;
                }
            } else {
                // Roman numeral;
                $index = strtoupper($index);
                if (preg_match(EXP_ROMAN_NUMERAL_PARTS, $index, $matches)) {
                    $item_index = 0;
                    array_shift($matches);
                    foreach ($matches as $key) {
                        if ($key) {
                            $item_index += self::$romanToDecimal[$key];
                        }
                    }
                } else {
                    $this->writeToLog('W', "invalid roman numeral");
                }
            }
            if (!$instance) {
                $instance = $parent_element->bodyAppend(new APListElement($render, $values));
                $this->lists[] = $instance;
            }
        } elseif (isset($values['label'])) {
            // Add list item or label.
            if (!$instance) {
                $instance = $parent_element->bodyAppend(new APListElement('labeledlist', $values));
                $this->lists[] = $instance;
            }
            $instance->addLabel($values['label']);
            #$this->listItemElement = null;
        } else {
            if (!$instance) {
                $instance = $parent_element->bodyAppend(new APListElement('bulletedlist', $values));
                $this->lists[] = $instance;
            }
        }

        $result2 = null;
        while ($this->reader->refNextLine() !== false) {
            if ($this->reader->currentLineIsBlank) {
                break;
            }
            if ($this->reader->currentLineText == '+') {
                $this->listContinuation = true;
                break;
            }
            $result2 = $this->getMatchResultFor('blockmacro|listdef|blockdef|attributelist');
            if ($result2) break;
            $text .= PHP_EOL . $this->reader->currentLineText;
            $result2 = null;
        }
        if ($text) {
            /*if (!($instance instanceOf APListElement)) {
                echo "** ".$instance->nodeName." ** ";
            }*/
            $instance->addListItem($text, $item_index);
        }
        if ($result2) {
            $this->processMatchResult($result2);
        }
    }

    /**
     * Detect whether current XML block is a list element.
     *
     * @return bool
     */
    function currentBlockIsList()
    {
        $node = $this->sectionElement->body->lastChild;
        if (!$node) return false;
        $list_elements = ['bulletedlist', 'calloutlist', 'labeledlist', 'numberedlist'];
        return in_array($node->nodeName, $list_elements) ? $node : false;
    }

    /**
     * Process delimited block.
     *
     * @param array $values
     *  Various values.
     */
    function processBlockdef(array $values)
    {
        $render = $values['render'];
        $block_end_found = false;
        $block_delimiter = $values['full_match'];
        $options = $values['options'] ?? null;
        $element = $this->createElement($render, null, $values);
        if (isset($values['name'])) {
            $element->setAttribute('name', $values['name']);
        }

        $base_line_num = $this->reader->getCurrentLineNumAbs();
        $block_end_found = false;
        $content = "";
        while ($this->reader->refNextLine() !== false) {
            if ($this->reader->currentLineText == $block_delimiter) {
                $block_end_found = true;
                break;
            }
            $content .= $this->reader->currentLineText . "\n";
        }
        if (!$block_end_found) {
            $this->writeToLog('E', "block end delimiter not found");
        }
        if ($options == 'sectionbody') {
            $this->process($element, $content, $base_line_num);
        } else {
            $element->body->textContent = $content;
        }
        $attachment_block = $this->getAttachmentElement();
        $attachment_block->bodyAppend($element);
        if ($this->detectListContinuationMarker($this->reader->nextLineText)) {
            $this->reader->refNextLine();
        }
    }

    /**
     * Render table.
     *
     * @param array $values
     *  Various values.
     */
    function processTabledef(array $values)
    {
        $base_line_num = $this->reader->getCurrentLineNumAbs() + 1;
        $end_delimiter = $values['full_match'];

        $def = $values['def'];
        $separator = $values['separator'];

        $block_end_found = false;
        $text = '';
        while ($this->reader->refNextLine() !== false) {
            if ($this->reader->currentLineText == $end_delimiter) {
                $block_end_found = true;
                break;
            }
            $text .= $this->reader->currentLineText . "\n";
        }
        if (!$block_end_found) {
            $this->writeToLog('E', "table end delimiter not found");
        }

        $format = $values['format'] ?? 'psv';
        if (!in_array($format, ['psv', 'dsv', 'csv', 'tsv'])) {
            $this->writeToLog('F', "no such table format: $format");
        }
        $element = new APTableElement($text, $values);
        $this->getAttachmentElement()->bodyAppend($element);
        return;
    }

    /**
     * Render paragraph.
     *
     * @param array $values
     *  Various values.
     */
    function processParadef(array $values)
    {
        if (!isset($values['render'])) return;
        $render = $values['render'];
        $text = $values['text'];
        $title = $values['title'] ?? null;
        $options = $values['options'] ?? null;
        $attachment_block = $this->getAttachmentElement(false);

        $result2 = null;
        while ($this->reader->refNextLine() !== false) {
            if ($this->detectListContinuationMarker($this->reader->currentLineText)) break;
            if ($this->reader->currentLineIsBlank) break;
            $result2 = $this->getMatchResultFor('blockdef|attributelist');
            if ($result2) break;
            $text .= PHP_EOL . $this->reader->currentLineText;
            $result2 = null;
        }
        if ($render == 'default') {
            $attachment_block->bodyAppendElement('para', $text, $values);
        } else {
            $element = $attachment_block->bodyAppendElement($render, null, $values);
            if (isset($values['name'])) {
                $element->setAttribute('name', $values['name']);
            }
            if ($options == 'sectionbody') {
                $element->bodyAppendElement('para', $text);
            } else {
                $element->body->textContent = $text;
            }
        }

        if (is_array($result2)) {
            $this->processMatchResult($result2);
        }
    }

    /**
     * Detect '+' at the end of a line of text.
     *
     * @param string $line_text
     *  Line of text.
     */
    private function detectListContinuationMarker(string $line_text)
    {
        if ($this->currentBlockIsList() and rtrim($line_text) == '+') {
            $this->listContinuation = true;
            return true;
        }
        return false;
    }

    /**
     * Parse attribute list.
     *
     * @param string $attr_list
     *  The attribute list string.
     */
    private function parseAttributeList(string $attr_list, &$block_attrs, &$pos_attrs = [])
    {
        if (!isset($block_attrs)) {
            $block_attrs = [];
        }
        $exp = '^(?J)(\s*(?<name>[a-z]+)\s*=)?(\s*"(?<value>[^"]*)"\s*(,|$)|(?<value>[^",]+)(,|$))';
        $pos = 0;
        while (strlen($attr_list) > 0) {
            $m = self::clean_match($exp, $attr_list);
            if (is_array($m)) {
                if (!empty($m['name'])) {
                    $block_attrs[$m['name']] = trim($m['value'] ?? '');
                } else {
                    $pos_attrs[$pos] = trim($m['value']);
                    $pos++;
                }
                $attr_list = substr($attr_list, strlen($m['full_match']));
            } else {
                $this->writeToLog('F', "cannot parse attribute list");
            }
        }
    }

    // Global attributes.

    /**
     * Get global attribute.
     *
     * @param string $name
     *  Attribute name.
     */
    function getGlobalAttr(string $name)
    {
        $attr = $this->globalAttributes[$name] ?? null;
        if ($attr and $attr['enabled'] == true) {
            return $attr['value'];
        }
    }

    /**
     * Set global attribute.
     *
     * @param string $name
     *  Attribute name.
     * @param string $value
     *  Attribute value.
     */
    function setGlobalAttr(string $name, string $value = null)
    {
        $enabled = true;
        if ($name[0] == '!' or $name[-1] == '!') {
            $name = trim($name, '!');
            $enabled = false;
        }
        if (!isset($this->globalAttributes[$name])) {
            $this->globalAttributes[$name] = ['enabled' => null, 'value' => null];
        }
        $this->globalAttributes[$name]['enabled'] = $enabled;
        if (isset($value)) {
            $this->globalAttributes[$name]['value'] = $value;
        }
    }

    /**
     * See whether a global attribute is enabled.
     *
     * @param string $name attribute name
     */
    function globalAttrEnabled(string $name)
    {
        return isset($this->globalAttributes[$name]) and $this->globalAttributes[$name]['enabled'];
    }

    /**
     * Create an XML element.
     *
     * @param string $name      element name
     * @param string $content   text content
     * @param array  $values    various values
     */
    function createElement($name, $content = null, array $values = null)
    {
        $original_content = $content;
        if ($content) {
            $style = $values['style'] ?? null;
            $subs = $values['subs'] ?? self::$substitutions['normal'];
            if (!empty($subs) and $style !== 'literal') {
                $subs_array = str_getcsv($subs);
                // Expand substitution keywords.
                foreach ($subs_array as $index => $entry) {
                    if (array_key_exists($entry, self::$substitutions)) {
                        array_splice($subs_array, $index, 1, str_getcsv(self::$substitutions[$entry]));
                    }
                }
                $subs_array = array_unique($subs_array);

                // Perform substitutions.
                foreach ($subs_array as $sub_type) {
                    switch ($sub_type) {
                        case 'specialcharacters':
                        #$content = $this->processSpecialChars($content);
                        $replacements = array('<' => '&lt;', '>' => '&gt;', '\\\\' => '&#92;');
                        foreach ($replacements as $source => $replacement) {
                            $content = str_replace($source, $replacement, $content);
                        }
                        if ($style !== 'literal') {
                            // Replace end of line + signs with breaks tags.
                            $content = preg_replace("/(?s)(\s{1}\+(\n|$))/m", '<br />', $content);
                        }
                        break;
                        case 'quotes':
                        $content = $this->processQuotes($content);
                        $content = str_replace('\\', '', $content);
                        break;
                        case 'replacements':
                        foreach ($this->configuration['replacements'] as $replacement => $exp) {
                            $replacement = rawurldecode($replacement);
                            $content = preg_replace("/$exp/", $replacement, $content);
                        }
                        break;
                        case 'macros':
                        $content = $this->macros($content);
                        break;
                    }
                }
                $content = trim($content, "\n");
                $frag = $this->createDocumentFragment();
                $frag->appendXML($content);
            }
        }

        $title = $values['title'] ?? null;
        $element = parent::createElement($name);
        if ($values) {
            $element->setAttributes($values);
        }
        if (isset(static::$elementInfo[$name])) {
            $element_info = static::$elementInfo[$name];
            switch($element_info['is']) {
                case 'formatted-text':
                if (isset($frag)) {
                    $element->appendChild($frag);
                }
                break;
                case 'raw-text':
                $element->textContent = $original_content;
                break;
                case 'titled-block':
                if (isset($title)) {
                    $element->appendElement('title', $title);
                }
                if (isset($frag)) {
                    $element->bodyAppend($frag);
                } elseif (is_string($content)) {
                    $element->body->textContent = $content;
                }
                if ($name == 'quoteblock' or $name == 'verseblock') {
                    if (isset($values['attribution'])) {
                        $attribution = $element->appendElement('attribution', $values['attribution']);
                        $attribution->textContent = $values['attribution']; // BODGE!
                        if (isset($values['citetitle'])) {
                            $attribution->appendElement('citetitle', $values['citetitle']);
                        }
                    }
                }
                break;
            }
        }
        return $element;
    }

    /**
     * Convert formatting characters into XML tags.
     *
     * @param string $content
     *  Content to process
     */
    function processQuotes(string $content)
    {
        $exp = '/' . implode('|', array_column($this->quotes, 'exp')) . '/s';
        $xml_string = $content;
        $xml_string = preg_replace_callback(
            $exp,
            function ($matches) {
                $def = null;
                $attributes = null;
                foreach ($matches as $key => $value) {
                    $found = false;
                    if (is_string($key) and !empty($value)) {
                        if ($key == 'attributes') {
                            $attributes = $value;
                        } else {
                            $def = $key;
                            $tag_content = $value;
                        }
                    }
                }
                if ($def) {
                    if (isset($attributes)) {
                        $attributes = preg_replace('/[\#?\.]+/', '', $attributes);
                        $attributes = preg_replace('/\s+/', ' ', $attributes);
                        $attributes = ' styling="' . $attributes . '"';
                    }
                    $def_num = intval(substr($def, 1));
                    $name = $this->quotes[$def_num]['tag_name'];
                    $flags = $this->quotes[$def_num]['flags'];
                    if (strpos($flags, 'l') === false) {
                        $tag_content = $this->processQuotes($tag_content);
                    }
                    return "<$name$attributes>$tag_content</$name>";
                }
            },
            $xml_string
        );
        return $xml_string;
    }

    function macros(string $xml_string)
    {
        $macros = $this->configuration['macros'];
        foreach ($macros as $name => $exp) {
            $xml_string = preg_replace_callback(
                "~$exp~s",
                function ($matches) use ($name) {
                    $values = ['full_match' => $matches[0]];
                    foreach ($matches as $key => $value) {
                        if (is_string($key) and !empty($value)) {
                            $values[$key] = $value;;
                        }
                    }
                    if ($values) {
                        if (isset($values['attrlist'])) {
                            $this->parseAttributeList($values['attrlist'], $block_attrs, $pos_attrs);
                            $values = array_merge($values, $block_attrs);
                        }
                        $name = $values['name'] ?? $name;
                        $target = $values['target'] ?? null;
                        $text = $pos_attrs[0] ?? null;
                        switch ($name) {
                            case 'http':
                            case 'https':
                            case 'ftp':
                            case 'file':
                            case 'mailto':
                            case 'callto':
                            $text = $pos_attrs[0] ?? ltrim($target, '/');
                            return self::createXMLElementString('link', $values, $text);
                            case 'image':
                            case 'xref':
                            case 'indexterm':
                            case 'indexterm2':
                            $text = $pos_attrs[0] ?? null;
                            return self::createXMLElementString($name, $values, $text);
                            case 'footnote':
                            case 'footnoteref':
                            $text = $values['attrlist'] ?? null;
                            $render = $text ? 'footnote' : 'footnote2';
                            return self::createXMLElementString($render, $values, $text);
                        }
                    }
                },
                $xml_string
            );
        }
        return $xml_string;
    }


    /**
     * Create an XML element in string form.
     *
     * @param string $name
     *  Element name
     */
    static function createXMLElementString(string $name, array $values = null, string $content = null)
    {
        $attrs = "";
        if ($values and isset(self::$elementInfo[$name]['attributes'])) {
            $attributes_allowed = str_getcsv(self::$elementInfo[$name]['attributes']);
            foreach ($attributes_allowed as $name_allowed) {
                if (isset($values[$name_allowed])) {
                    $value = $values[$name_allowed];
                    $attrs .= " $name_allowed=\"$value\"";
                }
            }
        }
        return "<$name$attrs>$content</$name>";
    }

    static function clean_match(string $exp, string $subject, bool $capture_offset = false)
    {
        $exp = "~$exp~";
        if (preg_match($exp, $subject, $match, $capture_offset ? PREG_OFFSET_CAPTURE : null)) {
            $values = [];
            $full_match = $match[0];
            if (is_array($full_match)) {
                $values['offset'] = $full_match[1];
                $full_match = $full_match[0];
            }
            $values['full_match'] = $full_match;
            foreach ($match as $key => $value) {
                if (is_string($key)) {
                    $value = is_array($value) ? $value[0] : $value;
                    if (!empty($value)) {
                        $values[$key] = $value;
                    }
                }
            }
            return $values;
        }

        return false;
    }

    /**
     * Return file contents.
     *
     * @param string $file_path path to file
     *
     * @return string           file contents
     */
    static function getFileContents(string $file_path)
    {
        $file_path_abs = __DIR__ . '/' . $file_path;
        if (!is_file($file_path_abs)) {
            self::errorLog('E', "file '$file_path' not found");
            return false;
        }
        if (!is_readable($file_path_abs)) {
            self::errorLog('E', "file '$file_path' not readable");
            return false;
        }
        return file_get_contents($file_path_abs);
    }

    /**
     * Write to system error log.
     *
     * @param string $status_code
     *  Log level code: 'W', 'E' or 'F'
     * @param string $message
     *  Message text
     * @param bool $include_line_number
     *  If true, include the line number the error occurred on. Default: true.
     */
    function writeToLog(string $status_code, string $message, bool $include_line_number = true)
    {
        $text = "";
        if (isset($this->filename)) {
            $text .= " '" . basename($this->filename) . "'";
        }
        if ($include_line_number) {
            $text .= " line " . strval($this->reader->getCurrentLineNumAbs() - 1);
        }
        $text .= ": $message.";
        self::errorLog($status_code, $text);
    }
/*
    function writeToLog(string $status_code, string $message, bool $include_line_number = true)
    {
        $statuses = ['W' => 'warning', 'E' => 'error', 'F' => 'fatal error'];
        $status = strtoupper($statuses[$status_code]);
        $text = "AsciiProc *$status*:";
        if (isset($this->filename)) {
            $text .= " '" . basename($this->filename) . "'";
        }
        if ($include_line_number) {
            $text .= " line " . strval($this->reader->getCurrentLineNumAbs() - 1);
        }
        $text .= ": $message.";
        $text = str_replace('::', ':', $text);
        error_log($text);
    }
*/
    /**
     * Write to system error log.
     *
     * @param string $status_code
     *  Log level code: 'W', 'E' or 'F'
     * @param string $message
     *  Message text
     */
    static function errorLog(string $status_code, string $message)
    {
        $statuses = ['W' => 'Warning', 'E' => 'Error', 'F' => 'Fatal error'];
        $status = $statuses[$status_code];
        $text = "AsciiProc $status:$message";
        $text = str_replace('::', ':', $text);
        error_log($text);
    }

    /*function appendChild(DOMNode $element)
    {
        parent::appendChild($element);
        if (method_exists($element, 'onAppend')) {
            $element->onAppend();
        }
    }

    function appendElement(string $name, $content = null, array $values = [])
    {
        $element = $this->createElement($name, $content, $values);
        $this->appendChild($element);
        return $element;
    }*/
}


// Functions.


function printout($input)
{
    echo '<pre>'; print_r($input); echo '</pre>';
}

/*
function sanitize_attribute_name($name)
{
    return preg_replace('/[^\w-]+/', '', strtolower(trim($name)));
}
*/
