<?php

require_once 'APTemplateLoaderStream.php';

class APTransformer extends XSLTProcessor
{
    /**
     * Constructor.
     */
    function __construct()
    {
        #$this->registerPHPFunctions('asciiproc_highlight');
        $this->registerPHPFunctions();

        // Register Stream Wrapper
        stream_wrapper_register('asciiproc', 'APTemplateLoaderStream');
        /*$opts = [
            'asciiproc' => [
                'namespaces' => $exsl['declarations'],
                'functions' => $exsl['functions']
        ];*/
        $opts = ['asciiproc' => []];
        $context = stream_context_create($opts);
        libxml_set_streams_context($context);
    }

    /**
     * @param string $file_path path to file
     */
    function importStyleSheetFromFile(string $file_path = null)
    {
        if (!$file_path) return false;
        if (!is_file($file_path)) return false;
        $stylesheet = new DOMDocument();
        $stylesheet->load($file_path);
        $this->importStylesheet($stylesheet);
    }

    function transformToXml($document)
    {
        $output = parent::transformToXml($document);
        if (trim(substr($output, 0, strpos($output, "\n"))) == '<!DOCTYPE html>') {
            $output = preg_replace('~\<meta http\-equiv="Content\-Type" content="text/html; charset=([^\"]+)"\>~', '<meta charset="$1">', $output);
        }
        return $output;
    }
}
