<?php

class APTemplateLoaderStream
{
    /**
     * @var int read position
     */
    private $position = 0;

    /**
     * @var string XSL template contents
     */
    private $template;

    /**
     * @param string $path stream path i.e. "asciiproc://functions"
     */
    function stream_open($path)#, $mode, $options, &$opened_path)
    {
        $url = parse_url($path);
        if ($url['host'] == 'functions') {
            $this->template = <<<'EOT'
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:func="http://exslt.org/functions"
    xmlns:php="http://php.net/xsl"
    xmlns:asciiproc="http://gitlab.com/peter-s/asciiproc"
    extension-element-prefixes="func php asciiproc">

    <func:function name="asciiproc:include">
        <xsl:param name="file_path"/>
        <func:result select="php:function('APDocument::getFileContents', $file_path)"/>
    </func:function>
    <func:function name="asciiproc:highlight">
        <xsl:param name="text"/>
        <xsl:param name="language"/>
        <xsl:param name="linenos"/>
        <func:result select="php:function('asciiproc_highlight', $text, $language)"/>
    </func:function>
</xsl:stylesheet>
EOT;
        }
        return true;
    }

    /**
     * @param int count number of bytes to return
     */
    function stream_read($count)
    {
        $pos = $this->position;
        $this->position += $count;
        return substr($this->template, $pos, $count);
    }

    function stream_write($data = null)
    {
        return 0;
    }

    function url_stat()
    {
        return [];
    }

    function stream_eof()
    {
        return true;
    }
}
